import { HeatMapContainer } from "./components/HeatMapContainer";

function App() {
  return (
    <div className="App">
      <HeatMapContainer />
    </div>
  );
}

export default App;
