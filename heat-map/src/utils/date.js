import { BASEHOURS, DAYS, HOURS } from "../consts/date";

export const dayToRow = {};
export const hourToColumn = {};

/**
 * Return date in H:M(AM/PM) format.
 * @param {Date} date Specific date from data.json file.
 */
export const formatAMPM = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
};

const ampmToindex = {
  AM: 0,
  PM: 12,
};
/**
 * Return date in H:M(AM/PM) format.
 * @param {String} time Specific time from data.json file.
 * @param {Object} hourToColumn Map between hour to column.
 */
export const getColumn = (time, hourToColumn) => {
  const splittedCurrentHour = time.split(" ");
  const when = splittedCurrentHour[1];
  const hourNumber = splittedCurrentHour[0].split(":");
  const fullHour = hourNumber[0] + when;
  const column = hourToColumn[fullHour];
  if (column === undefined) {
    let indexOfCellToCheck =
      ampmToindex[when] +
      (hourNumber[0] === "12" ? 0 : parseInt(hourNumber[0])) -
      1;
    while (!HOURS.includes(BASEHOURS[indexOfCellToCheck])) {
      indexOfCellToCheck -= 1;
      if (indexOfCellToCheck < 0) {
        indexOfCellToCheck = BASEHOURS.length - 1;
      }
    }
    return hourToColumn[BASEHOURS[indexOfCellToCheck]];
  } else {
    return column;
  }
};
/**
 * Return row and column according to specific date.
 * @param {String} date Specific date from data.json file.
 */
export const parseDate = (date) => {
  const currentDate = new Date(date);
  const dateDayName = DAYS[currentDate.getDay()];
  const dateHour = formatAMPM(currentDate);
  const row = dayToRow[dateDayName];
  const column = getColumn(dateHour, hourToColumn);
  return { row, column };
};
