import axios from "axios";
import React, { useEffect, useState } from "react";
import { COLORS, EXCLUDECOLOR } from "../consts/colors";
import { DAYS, HOURS } from "../consts/date";
import { dayToRow, hourToColumn, parseDate } from "../utils/date";
import { HeatMapDisplay } from "./HeatMapDisplay";

const FILE_NAME = "data.json";
const HEIGHT = 45;
const FONT_SIZE = "13px";

const heatMapStyle = {
  margin: "18px",
  height: 10,
  width: 10,
  borderRadius: "360%",
};

/**
 * Container component for parsing necessary data for the heat map.
 */
export const HeatMapContainer = () => {
  const [colorsMap, setColorsMap] = useState([]);
  const [maxActivities, setMaxActivities] = useState(0);

  const [heatMapDataActiveStatus, setHeatMapDataActiveStatus] = useState(
    new Array(DAYS.length).fill(0).map(() => new Array(HOURS.length).fill(true))
  );
  const [heatMapData, setHeatMapData] = useState(
    new Array(DAYS.length).fill(0).map(() => new Array(HOURS.length).fill(0))
  );
  /**
   * Create mapping between day and row, time and column.
   */
  const createMaps = () => {
    DAYS.forEach((day, index) => {
      dayToRow[day] = index;
    });

    HOURS.forEach((hour, index) => {
      hourToColumn[hour] = index;
    });
  };

  useEffect(() => {
    getData();
  }, []);

  /**
   * Get dates from data.json.
   */
  const getData = () => {
    axios
      .get(FILE_NAME)
      .then((res) => {
        fillActivitiesInArray(res.data);
      })
      .catch((err) => console.log(err));
  };

  /**
   * Parse each activity and fill in array.
   * @param  {[String]} dates The dates from the data.json file.
   */
  const fillActivitiesInArray = (dates) => {
    let maxActivities = 0;
    const initialHeatMapData = new Array(DAYS.length)
      .fill(0)
      .map(() => new Array(HOURS.length).fill(0));

    dates.forEach((date) => {
      const { row, column } = parseDate(date);
      initialHeatMapData[row][column] += 1;
      const curretCellActivities = initialHeatMapData[row][column];
      if (
        curretCellActivities > maxActivities &&
        heatMapDataActiveStatus[row][column]
      ) {
        maxActivities = curretCellActivities;
      }
    });
    setMaxActivities(maxActivities);
    setHeatMapData(initialHeatMapData);
  };

  useEffect(() => {
    const lengthOfColors = COLORS.length;
    const partitionSize = maxActivities / (lengthOfColors - 1);
    let colorsMap = [];
    let partitionsSum = partitionSize;

    for (let colorIndex = 1; colorIndex < lengthOfColors; colorIndex++) {
      colorsMap.push({
        maxActivities: partitionsSum,
        color: COLORS[colorIndex],
      });
      partitionsSum += partitionSize;
    }
    setColorsMap(colorsMap);
  }, [maxActivities]);

  useEffect(() => {
    setHeatMapData(heatMapData);
  }, [colorsMap]);

  /**
   * For each bucket, return the appropriate color according to the amount of activities.
   * @param  {Number} row The row index of current bucket.
   * @param  {Number} col The column index of current bucket.
   */
  const getColorByActivities = (row, col) => {
    if (!heatMapDataActiveStatus[row][col]) {
      return EXCLUDECOLOR;
    }
    if (heatMapData[row][col] === 0) {
      return COLORS[0];
    }
    for (let colorIndex = 0; colorIndex < colorsMap.length; colorIndex++) {
      if (heatMapData[row][col] <= colorsMap[colorIndex].maxActivities) {
        return colorsMap[colorIndex].color;
      }
    }
  };

  /**
   * Exclude current bucket from the heat map.
   * @param  {Number} row The row index of current bucket.
   * @param  {Number} col The column index of current bucket.
   */
  const toggleExcludeActivities = (row, col) => {
    const tempHeatMapDataActiveStatus = [...heatMapDataActiveStatus];
    tempHeatMapDataActiveStatus[row][col] =
      !tempHeatMapDataActiveStatus[row][col];
    setHeatMapDataActiveStatus(tempHeatMapDataActiveStatus);
  };

  useEffect(() => {
    createMaps();
    updateMaxActivities();
  }, [heatMapDataActiveStatus]);

  /**
   * Calculate the highest amount of activities.
   */
  const updateMaxActivities = () => {
    let maxActivities = 0;
    const tempHeatMapData = [...heatMapData];
    tempHeatMapData.forEach((row, rowIndex) => {
      row.forEach((col, colIndex) => {
        if (
          tempHeatMapData[rowIndex][colIndex] > maxActivities &&
          heatMapDataActiveStatus[rowIndex][colIndex]
        ) {
          maxActivities = tempHeatMapData[rowIndex][colIndex];
        }
      });
    });
    setMaxActivities(maxActivities);
  };

  return (
    <div style={{ fontSize: FONT_SIZE }}>
      <HeatMapDisplay
        xLabels={HOURS}
        yLabels={DAYS}
        xLabelLocation={"bottom"}
        heatMapData={heatMapData}
        colorByValueCallback={getColorByActivities}
        onCellClickCallback={toggleExcludeActivities}
        cellStyle={heatMapStyle}
        height={HEIGHT}
      />
    </div>
  );
};
