import React from "react";
import HeatMap from "react-heatmap-grid";

/**
 * Generic component for displaying a heat map.
 */
export const HeatMapDisplay = (props) => {
  const {
    xLabels,
    yLabels,
    xLabelLocation,
    heatMapData,
    colorByValueCallback,
    onCellClickCallback,
    height,
    cellStyle,
  } = {
    ...props,
  };

  return (
    <HeatMap
      xLabels={xLabels}
      yLabels={yLabels}
      xLabelsLocation={xLabelLocation}
      data={heatMapData}
      squares
      height={height}
      onClick={(x, y) => onCellClickCallback(y, x)}
      cellStyle={(background, value, min, max, data, x, y) => ({
        background: colorByValueCallback(y, x),
        ...cellStyle,
      })}
    />
  );
};
