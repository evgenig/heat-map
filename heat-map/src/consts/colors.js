/** @constant
    Configurable colors for the heat map.
*/
export const COLORS = [
  "rgb(244,244,244)",
  "rgb(102,173,246)",
  "rgb(154,201,64)",
  "rgb(253,189,65)",
  "rgb(253,161,62)",
  "rgb(238,51,56)",
];

export const EXCLUDECOLOR = "rgb(255, 255, 255)";
